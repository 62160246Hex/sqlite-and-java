/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksarak.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author LagunaZi
 */
public class InsertUser {
        public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            
            String sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (4, 'raiden', 'password55');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (5, 'Kujoh', 'password54');";
            stmt.executeUpdate(sql);
            
            sql = "INSERT INTO USER (ID,USERNAME,PASSWORD) "
                    + "VALUES (6, 'Kokomi', 'password53');";
            stmt.executeUpdate(sql);
            
            conn.commit();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlite.JDBC not found !!! ");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database!!!");
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }
    
}
